# Videogame-Affordances-Corpus

A collection of images from video games semantically labelled according to 9 object interaction affordances. Intended for use in training and transfer learning for machine learning systems involving video game graphics.

## Table of Contents

- [Installation](#installation)
- [Background](#background)
- [Usage](#usage)
- [Support](#support)
- [Contributing](#contributing)

## Installation
This assumes you have some conda installation functioning. Simplest way to get this running: [https://docs.conda.io/en/latest/miniconda.html]. On Mac download python 3.7 bash installer. Assuming this goes to your Downloads folder, do the following in a Terminal shell and follow the prompts. After installing open a fresh Terminal and try `conda -V`

```
cd Downloads
bash Miniconda3-latest-MacOSX-x86-64.sh
```

To get the dataset and tagging tool, the following should be the simplest way to get up and running
```
git clone https://github.com/gerardrbentley/Videogame-Affordances-Corpus.git your_folder_name

cd your_folder_name/tagging_tool

conda env create --name your_env_name --file start_env.yml

conda activate your_env_name

python main.py --game-dir loz --file-num 0
```


## Background

More info to come after publication

## Usage


Set up to automatically progress through img folder for a given game-dir, use ctrl-c
  to exit python (may have to close gui window as well)
  Change this by uncommenting final break line or remove loop entirely in main

Keybindings:
  Toggle affordances for the currently selected areas (cyan for tiles, red for user drawn box) using q-w-e a-s-d z-x-c or 7-8-9 4-5-6 1-2-3.

  Enter/Return will Overwrite selected areas with the selected afordances, and if the selected areas are cyan/game tiles, the tile image and affordances will be saved to tile_img/ and tile_affordances.csv

  Shift will Fill in the unknown/gray/0.5 value pixels in selected area without overwriting any previously set affordances. This will NOT save a tile to the folder (inteded for tiles that are overlapped by a sprite but not handled by system)

  Click and drag to select an area to tag

  Escape will close current window and move on to next image

  Arrow keys are used to shift the grid for tile matching when it isn't aligned with 0,0. Toggle grid button to see it, redo matching button to repeat tile matching on the corrected grid

## Support

## Contributing
