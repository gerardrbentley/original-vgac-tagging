import pandas as pd
import os
import cv2
import glob
import numpy as np

AFFORDANCES = ["solid", "movable", "destroyable",
               "dangerous", "gettable", "portal", "usable", "changeable", "ui"]
AFFORDANCE_CONTROLS = {'q': 0, 'w': 1, 'e': 2, 'a': 3, 's': 4, 'd': 5, 'z': 6, 'x': 7,
                       'c': 8, '1': 6, '2': 7, '3': 8, '4': 3, '5': 4, '6': 5, '7': 0, '8': 1, '9': 2}
UI_AFFORDANCE = [0, 0, 0, 0, 0, 0, 0, 0, 1]

DEFAULTS = {'loz': {'ui_height': 56, 'grid_size': 16, 'ui_position': 'top'},
            'sm3': {'ui_height': 40, 'grid_size': 8, 'ui_position': 'bot'},
            'metroid': {'ui_height': 0, 'grid_size': 16, 'ui_position': 'top'}
            }

E = 1e-8


class Observable:
    def __init__(self, initialValue=None):
        self.data = initialValue
        self.callbacks = {}

    def addCallback(self, func):
        self.callbacks[func] = 1

    def delCallback(self, func):
        del self.callback[func]

    def _docallbacks(self):
        for func in self.callbacks:
             func(self.data)

    def set(self, data):
        self.data = data
        self._docallbacks()

    def get(self):
        return self.data

    def unset(self):
        self.data = None


def load_templates(dir):
    templates = {}

    for file in glob.glob(os.path.join("..", 'games', dir, 'tile_img', '*.png')):
        file_name = os.path.split(file)[1]
        name = os.path.splitext(file_name)[0]
        img = cv2.imread(file, cv2.IMREAD_UNCHANGED)
        channels = cv2.split(img)
        if(len(channels) == 1):
            img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        templates[int(name)] = img

    affords = pd.read_csv(os.path.join(
         '..', 'games', dir, 'tile_affordances.csv'))
    return templates, affords


def load_sprites_and_masks(dir):
    sprites = {}
    flag = True
    for file in glob.glob(os.path.join("..", 'games', dir, 'sprite', '*.png')):
        file_name = os.path.split(file)[1]
        name = os.path.splitext(file_name)[0]
        img = cv2.imread(file, cv2.IMREAD_UNCHANGED)
        channels = cv2.split(img)
        if(len(channels) == 1):
            img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGRA)
            channels = cv2.split(img)
        if(len(channels) == 3):
            img = cv2.cvtColor(img, cv2.COLOR_BGR2BGRA)
            channels = cv2.split(img)
        # if flag:
        #     print('unique values insprite {}'.format(np.unique(img)))
        #     flag = False
        gray_sprite = cv2.cvtColor(img, cv2.COLOR_BGRA2GRAY)
        img = (32.0 * (img // (32.0))).astype('uint8')
        zero_channel = np.zeros_like(channels[0], np.uint8)
        mask = np.array(channels[3])
        mask[channels[3] == 0] = 0
        mask[channels[3] == 255] = 255

        grad_x = cv2.Sobel(gray_sprite, cv2.CV_64F, 1, 0, ksize=3)
        grad_y = cv2.Sobel(gray_sprite, cv2.CV_64F, 0, 1, ksize=3)

        abs_grad_x = cv2.convertScaleAbs(grad_x)
        abs_grad_y = cv2.convertScaleAbs(grad_y)

        sobel_sp = cv2.addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0)
        dst = cv2.Laplacian(gray_sprite, cv2.CV_64F, ksize=3)
        laplace_sp = cv2.convertScaleAbs(dst)

        canny_sp = cv2.Canny(gray_sprite, 10, 300)

        #img = cv2.cvtColor(img, cv2.COLOR_BGRA2GRAY)
        alpha_mask = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGRA)
        sprites[int(name)] = (img, alpha_mask, gray_sprite,
                              mask, sobel_sp, laplace_sp, canny_sp)
    affords = pd.read_csv(os.path.join(
         '..', 'games', dir, 'sprite_affordances.csv'))
    return sprites, affords


def gen_grid(width, height, grid_size, ui_height=0, ui_position='top', grid_offset_x=0, grid_offset_y=0):
    if ui_position == 'top':
        ignore_start = ui_height
    else:
        ignore_start = 0
    row_num = (height - ui_height) // grid_size
    if (row_num * grid_size) + ignore_start + grid_offset_y >= height:
        row_num -= 1
    col_num = (width) // grid_size
    if (col_num * grid_size) + grid_offset_x > width:
        col_num -= 1

    rows, cols = np.indices((row_num, col_num))
    rows = rows * grid_size
    cols = cols * grid_size

    rows = rows + ignore_start + grid_offset_y
    cols = cols + grid_offset_x

    return rows, cols


def point_on_grid(c, r, cols, rows):
    return c in cols and r in rows
