from PIL import ImageTk, Image
import tkinter as tk

import utils


class ChannelWidget(tk.Toplevel):
    def __init__(self, root):
        tk.Toplevel.__init__(self, root)
        # self.stacked_np = self.stacked_tensor.numpy()
        # self.stacked_np = (self.stacked_np * 255).astype(np.uint8)
        self.content = tk.Frame(self)
        # self.channels = []
        self.channel_labels = []

        for x in range(len(utils.AFFORDANCES)):
            self.channel_labels.append(
                tk.Label(self.content, image=None))
            r = int(x / 3) + 1
            c = int(x % 3)
            # self.channel_labels[x].pack()
            self.channel_labels[x].grid(column=c, row=r)

        self.content.pack(fill=tk.X, expand=True)

    def RefreshDisplayChannels(self, stacked_np):
        print('Update Display channels {}'.format(stacked_np.shape))
        for x in range(len(utils.AFFORDANCES)):
            channel = stacked_np[:, :, x]
            channel_PIL = Image.fromarray(channel, mode='L')
            channel_TK = ImageTk.PhotoImage(channel_PIL)
            self.channel_labels[x].configure(image=channel_TK)
            self.channel_labels[x].image = channel_TK


class GridControlsWidget(tk.Frame):
    def __init__(self, root):
        tk.Frame.__init__(self, root)

        self.reset_button = tk.Button(
                       self, text="REDO matching")
        self.reset_button.grid(sticky='nsew', column=2, row=0)
        # self.GRID_ON = False
        self.toggle_button = tk.Button(
                       self, text="toggle grid")
        self.toggle_button.grid(sticky='nsew', column=0, row=0)

        self.up_button = tk.Button(self, text="grid up")
        self.down_button = tk.Button(self, text="grid down")
        self.left_button = tk.Button(self, text="grid left")
        self.right_button = tk.Button(self, text="grid right")

        self.up_button.grid(sticky='nsew', column=1, row=0)
        self.down_button.grid(sticky='nsew', column=1, row=1)
        self.left_button.grid(sticky='nsew', column=0, row=1)
        self.right_button.grid(sticky='nsew', column=2, row=1)


class CheckBoxesWidget(tk.Frame):
    def __init__(self, root):
        tk.Frame.__init__(self, root)
        self.affordance_checkboxes = []

        for i in range(9):
            self.affordance_checkboxes.append(tk.Checkbutton(
                self, text=utils.AFFORDANCES[i]))
            r = int(i / 3) + 1
            c = int(i % 3)
            # self.affordance_checkboxes[i].pack(fill=tk.X, expand=True)
            self.affordance_checkboxes[i].grid(sticky='nsew', column=c, row=r)
        # self.pack(fill=tk.X, expand=True)

    def FlipAffordanceCheckbox(self, idx):
        print('Update Affordance Checkbox: ', idx, utils.AFFORDANCES[idx])
        self.affordance_checkboxes[idx].toggle()

    def AllOff(self):
        for i in range(len(self.affordance_checkboxes)):
            self.affordance_checkboxes[i].deselect()


class ThumbnailWidget(tk.Label):
    def __init__(self, root):
        tk.Label.__init__(self, root)
        self.thumbnail_img = None

    def UpdateThumbnailImage(self, new_image):
        print('Update Thumbnail Image {}'.format(new_image.size))
        temp = new_image.resize(
             (new_image.size[0]*4, new_image.size[1]*4))
        self.thumbnail_img = ImageTk.PhotoImage(temp)
        self.configure(image=self.thumbnail_img)


class MainImageWidget(tk.Frame):
    def __init__(self, root):
        tk.Frame.__init__(self, root)

        self.display_canvas = tk.Canvas(self,  width=256, height=224)
        self.img_tk = None
        self.display_canvas.pack(fill=tk.X, expand=True, side=tk.LEFT)
        self.GRID_ON = False

    def InitDisplayImage(self, new_image):
        print('Init Display Image {}'.format(new_image.size))
        self.img_tk = ImageTk.PhotoImage(new_image)
        self.display_canvas.create_image(0, 0, image=self.img_tk, anchor='nw')

    def RefreshDisplayImage(self, new_image):
        print('Update Display Image {}'.format(new_image.size))
        self.img_tk = ImageTk.PhotoImage(new_image)
        self.display_canvas.configure(image=self.img_tk)

    def DrawUserBox(self, x0, y0, x1, y1):
        self.display_canvas.delete('userbox')
        self.display_canvas.create_rectangle(
            (x0, y0, x1, y1), width=3, fill='', tags=('userbox'), outline='red')

    def UpdateUserBox(self, x0, y0, x1, y1):
        self.display_canvas.coords(
            'userbox', x0, y0, x1, y1)

    def DrawBoxes(self, locations, image_shape, color='cyan', tag='bbox'):
        if tag == 'bbox':
            self.display_canvas.delete(tag)
        for loc in locations:
            # print('draw bb at ', loc, image_shape)
            self.display_canvas.create_rectangle(
                (loc[1], loc[0], loc[1]+16, loc[0]+16), width=4, fill='', tags=(tag), outline=color)

    def HideGrid(self):
        self.display_canvas.delete('grid_lines')

    def ShowGrid(self, height, width, grid_size, grid_offset_x, grid_offset_y):
        # if self.GRID_ON:
        #     self.GRID_ON = False
        self.display_canvas.delete('grid_lines')

        # self.GRID_ON = True
        r_range = range(grid_offset_y, height, grid_size)
        c_range = range(grid_offset_x, width, grid_size)
        # if self.UI_POSITION == 'top':
        #     r_range = range((self.UI_HEIGHT+self.GRID_OFFSET_Y)*2,
        #                     self.height*2, self.GRID_SIZE*2)
        # else:
        #     r_range = range(2*self.GRID_OFFSET_Y, (self.height
        #                                            - self.UI_HEIGHT)*2, self.GRID_SIZE*2)
        for r in r_range:
            self.display_canvas.create_line(
                0, r, width, r, fill='red', width=1, tags=('grid_lines'))
        for c in c_range:
            self.display_canvas.create_line(
                c, 0, c, height, fill='red', width=1, tags=('grid_lines'))
