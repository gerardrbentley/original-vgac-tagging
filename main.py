
from PIL import Image, ImageTk
import cv2
import tkinter as tk
import torch
import numpy as np
import pandas as pd

import utils
from utils import Observable

# from affordance_analyzer import AffordanceModel
from widgets import GridControlsWidget, ChannelWidget, CheckBoxesWidget, ThumbnailWidget, MainImageWidget

import os
import argparse

'''
Main Controller class, handles user input and coordinates affordance and image info with
 TK widgets / views
'''


class AffordanceTagger(tk.Tk):
    def __init__(self, args):
        tk.Tk.__init__(self)
        self.data_path = args.data_path
        self.game_dir = args.game_dir

        #Data Model
        self.affordance_data = AffordanceAnalyzer(args)
        self.affordance_data.display_image.addCallback(self.RefreshDisplay)
        self.affordance_data.display_channels.addCallback(
            self.UpdateAffordances)
        self.affordance_data.thumbnail_image.addCallback(self.UpdateThumbnail)

        #Data for Controller
        self.tagging_tile = True
        self.new_tile_idx = -1
        self.grid_on = False
        self.affordance_input = [0, 0, 0, 0, 0, 0, 0, 0, 0]

        #Views
        self.main_view = MainImageWidget(self)
        self.InitDisplay(self.affordance_data.display_image.get())
        self.DrawSpriteBoxes(
            self.affordance_data.sprite_locations_and_sizes.get())

        self.affordances_view = CheckBoxesWidget(self)

        self.thumbnail_view = ThumbnailWidget(self)
        self.UpdateThumbnail(self.affordance_data.thumbnail_image.get())

        self.channel_view = ChannelWidget(self)
        self.UpdateAffordances(self.affordance_data.display_channels.get())

        self.grid_updater_view = GridControlsWidget(self)
        self.grid_updater_view.toggle_button.config(command=self.toggle_grid)
        self.grid_updater_view.reset_button.config(command=self.RedoMatching)
        self.grid_updater_view.up_button.config(command=self.shift_grid_up)
        self.grid_updater_view.down_button.config(command=self.shift_grid_down)
        self.grid_updater_view.left_button.config(command=self.shift_grid_left)
        self.grid_updater_view.right_button.config(
            command=self.shift_grid_right)

        self.init_gui()
        self.bind_keys()

        self.NextTile(None)

    '''
    Takes in PIL image and presents it in the main image canvas
    '''

    def InitDisplay(self, new_image):
        self.main_view.InitDisplayImage(new_image)

    '''
    Takes in PIL image and updates the main image canvas
    '''

    def RefreshDisplay(self, new_image):
        self.main_view.RefreshDisplayImage(new_image)

    '''
    Takes in a list of pairs of locations and shapes
    locations are (y,x) coords and shapes are (h,w) tuples/np.array
    [((y,x), (h,w)), ...]
    '''

    def DrawSpriteBoxes(self, locs_sizes):
        for (loc, shape) in locs_sizes:
            self.main_view.DrawBoxes([loc], shape, color='green', tag='sprite')

    '''
    Takes in a PIL image to scale up 4x size and updates the thumbnail view
    '''

    def UpdateThumbnail(self, new_image):
        self.thumbnail_view.UpdateThumbnailImage(new_image)
        # new_locations = self.affordance_data.thumbnail_locations.get()

    '''
    Gets the next new
    '''

    def NextTile(self, event):
        unknown_tiles = self.affordance_data.new_tiles.get()
        if len(unknown_tiles) > 0:
            self.new_tile_idx += 1
            if self.new_tile_idx == len(unknown_tiles):
                self.new_tile_idx = 0

            tile, locations = unknown_tiles[self.new_tile_idx]

            self.affordance_data.askAffordances(tile, locations[0])
            self.main_view.DrawBoxes(locations, tile.shape)
        else:
            print('NO NEW TILES')
        # self.UpdateThumbnail(tile)

    def SaveTileInfo(self):
        idx, file_num = self.affordance_data.insert_in_csv(
            self.affordance_input, self.game_dir, self.data_path)
        self.affordance_data.thumbnail_image.get().save(os.path.join(
            self.data_path, self.game_dir, 'tile_img', str(file_num)+'.png'), compression=0)
        print('saved afford and image to disk ', os.path.join(
            self.data_path, self.game_dir, 'tile_img', str(file_num)+'.png'), 'idx', idx)

    def FlipAffordance(self, idx):
        # self.affordance_data.flipAffordance(idx)
        self.affordance_input[idx] = (self.affordance_input[idx] + 1) % 2
        self.affordances_view.FlipAffordanceCheckbox(idx)

    def UpdateAffordances(self, channel_data):
        # channel_data = self.affordance_data.display_channels.get().numpy()
        channel_data = (channel_data.numpy() * 255).astype(np.uint8)
        self.channel_view.RefreshDisplayChannels(channel_data)

    def ApplyAffordances(self, overwrite=False):
        if self.tagging_tile:
            unknown_tiles = self.affordance_data.new_tiles.get()
            tile, locations = unknown_tiles[self.new_tile_idx]
            self.affordance_data.set_affordances_at_locations(
                self.affordance_input, locations, tile.shape, overwrite=overwrite)
            if overwrite:
                self.SaveTileInfo()
            self.NextTile(None)
        else:
            self.affordance_data.set_affordances_at_locations(
                self.affordance_input, [(self.y0, self.x0)], (self.y1-self.y0, self.x1-self.x0), overwrite=overwrite)
            pass
            # Get bbox from canvas and apply affordances by hand
            # self.affordance_data.confirmAffordances(self.affordance_input)
            # self.UpdateAffordances()
        self.affordance_input = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        self.affordances_view.AllOff()

    def ConfirmAffordances(self, event):
        self.ApplyAffordances(overwrite=True)

    def FillInAffordances(self, event):
        self.ApplyAffordances(overwrite=False)

    def RedoMatching(self):
        self.new_tile_idx = -1
        self.affordance_data.redo_template_match()
        self.NextTile(None)

    def keystroke(self, event):
        e = event.char
        if e in utils.AFFORDANCE_CONTROLS.keys():
            print('input: ', e, '. switch: ', utils.AFFORDANCE_CONTROLS[e])
            self.FlipAffordance(utils.AFFORDANCE_CONTROLS[e])

    # Determine the origin of bounding box by clicking
    def getorigin(self, eventorigin):
        self.x0 = self.x1 = eventorigin.x
        self.y0 = self.y1 = eventorigin.y

        print(self.y0, self.x0)
        self.main_view.DrawUserBox(self.x0, self.y0, self.x1, self.y1)
        # self.image.delete('bbox')
        # self.image.create_rectangle(
        #     (self.x0*2, self.y0*2, self.x1*2, self.y1*2), width=3, fill='', tags=('bbox'), outline='red')

    def on_drag(self, eventorigin):
        self.x1, self.y1 = (eventorigin.x, eventorigin.y)
        # if self.x1 > self.width:
        #     self.x1 = self.width
        # if self.y1 > self.height:
        #     self.y1 = self.height
        # self.image.coords('bbox', self.x0*2, self.y0*2, self.x1*2, self.y1*2)
        self.main_view.UpdateUserBox(self.x0, self.y0, self.x1, self.y1)
        self.tagging_tile = False
        # self.root.bind(
        #     "<Return>", lambda event: self.manual_affordances(event, force=True))

    def on_release(self, eventorigin):
        pass

    def toggle_grid(self):
        if self.grid_on:
            self.grid_on = False
            self.main_view.HideGrid()
        else:
            self.grid_on = True
            self.set_grid()

    def set_grid(self):
        height = self.affordance_data.HEIGHT
        width = self.affordance_data.WIDTH
        grid_size = self.affordance_data.GRID_SIZE
        grid_offset_x = self.affordance_data.GRID_OFFSET_X
        grid_offset_y = self.affordance_data.GRID_OFFSET_Y
        if self.affordance_data.UI_POSITION == 'top':
            ignore_start = self.affordance_data.UI_HEIGHT
        else:
            ignore_start = 0
        self.main_view.ShowGrid(height, width, grid_size,
                                grid_offset_x, grid_offset_y+ignore_start)

    def shift_grid_up(self, event=None):
        self.affordance_data.GRID_OFFSET_Y = self.affordance_data.GRID_OFFSET_Y - \
            1 if self.affordance_data.GRID_OFFSET_Y > 0 else 0
        # self.toggle_grid()
        # self.toggle_grid()
        self.set_grid()

    def shift_grid_left(self, event=None):
        self.affordance_data.GRID_OFFSET_X = self.affordance_data.GRID_OFFSET_X - \
            1 if self.affordance_data.GRID_OFFSET_X > 0 else 0
        # self.toggle_grid()
        # self.toggle_grid()
        self.set_grid()

    def shift_grid_right(self, event=None):
        self.affordance_data.GRID_OFFSET_X = self.affordance_data.GRID_OFFSET_X + 1
        # self.toggle_grid()
        # self.toggle_grid()
        self.set_grid()

    def shift_grid_down(self, event=None):
        self.affordance_data.GRID_OFFSET_Y = self.affordance_data.GRID_OFFSET_Y + 1
        # self.toggle_grid()
        # self.toggle_grid()
        self.set_grid()

    def init_gui(self):
        self.main_view.pack(fill=tk.X, side=tk.LEFT, expand=True)
        self.thumbnail_view.pack(fill=tk.X, side=tk.LEFT, expand=True)
        self.affordances_view.pack(fill=tk.X, side=tk.LEFT, expand=True)
        self.grid_updater_view.pack(fill=tk.X, side=tk.LEFT, expand=True)

    def bind_keys(self):
        self.main_view.display_canvas.bind("<ButtonPress-1>", self.getorigin)
        self.main_view.display_canvas.bind("<B1-Motion>", self.on_drag)
        self.main_view.display_canvas.bind(
            "<ButtonRelease-1>", self.on_release)
        self.bind("<Return>", self.ConfirmAffordances)
        self.bind("<space>", self.NextTile)
        self.bind("<Key>", self.keystroke)
        self.bind("<Shift_L>", self.FillInAffordances)
        self.bind("<Up>", self.shift_grid_up)
        self.bind("<Down>", self.shift_grid_down)
        self.bind("<Left>", self.shift_grid_left)
        self.bind("<Right>", self.shift_grid_right)
        self.bind("<Escape>", self.close_window)

    def close_window(self, event=None):
        self.destroy()

    def run(self):
        self.title("Affordance Tagger")
        self.deiconify()
        self.mainloop()


'''
Contains the underlying affordance and image data for analyzing / preprocessing

'''


class AffordanceAnalyzer:
    def __init__(self, args):
        self.game_dir = args.game_dir
        self.data_path = args.data_path
        self.image_path = os.path.join(
            self.data_path, self.game_dir, 'img', str(args.file_num)+'.png')

        if args.ui_position == 'default':
            self.UI_POSITION = utils.DEFAULTS[self.game_dir]['ui_position']
        else:
            self.UI_POSITION = args.ui_position.lower()

        if args.ui_height == -1:
            self.UI_HEIGHT = utils.DEFAULTS[self.game_dir]['ui_height']
        else:
            self.UI_HEIGHT = args.ui_height

        if args.grid_size == -1:
            self.GRID_SIZE = utils.DEFAULTS[self.game_dir]['grid_size']
        else:
            self.GRID_SIZE = args.grid_size
        self.GRID_OFFSET_X, self.GRID_OFFSET_Y = (0, 0)

        self.display_image = Observable(None)
        self.display_channels = Observable(None)
        self.unique_tiles = Observable(None)
        self.new_tiles = Observable(None)
        self.thumbnail_image = Observable(None)
        self.sprite_locations_and_sizes = Observable(None)
        # self.thumbnail_locations = Observable(None)
        self.curr_tile_num = Observable(0)

        self.loadImage(self.image_path)
        self.init_filtered_images(self.image_path)
        self.loadTemplates(self.game_dir)

        self.rows, self.cols = utils.gen_grid(
            self.WIDTH, self.HEIGHT, self.GRID_SIZE, self.UI_HEIGHT, self.UI_POSITION,
            self.GRID_OFFSET_X, self.GRID_OFFSET_Y)

        self.detect_sprites()
        self.find_unique_tiles()

        self.match_known_tiles()

        self.tag_ui()

        self.tag_border_portals()

        # self.affordance_vars = []
        # for i in range(len(utils.AFFORDANCES)):
        #     self.affordance_vars.append(0)
        # self.curr_affordances = Observable(self.affordance_vars)

        # self.askAffordances()

    def loadImage(self, image_path):
        file_name = os.path.split(image_path)[1]
        num = os.path.splitext(file_name)[0]

        # Loads in as 8 bit unknown color palette
        self.orig_PIL = Image.open(image_path).convert(mode="RGB")
        self.WIDTH, self.HEIGHT = self.orig_PIL.size
        self.display_image.set(self.orig_PIL.copy())
        self.thumbnail_image.set(self.orig_PIL.crop(box=(0, 0, 16, 16)))

        self.label_file = image_path.replace(
            'img', 'label').replace('png', 'pt')

        # Load or create 224x256x9 torch tensor for afforance data
        #TODO: Explore different formats (sparseTensor, txt, binary)
        if os.path.isfile(self.label_file):
            print('Label File Found')
            self.match_all_old = False
            self.stacked_tensor = torch.load(self.label_file)
        else:
            print('New Label File')
            self.match_all_old = True
            self.stacked_tensor = torch.full(
                [224, 256, 9], fill_value=0.5, dtype=torch.float32)

        self.stacked_tensor.requires_grad_(False)
        self.display_channels.set(self.stacked_tensor)

    def loadTemplates(self, game_dir):
        self.templates, self.template_affordances = utils.load_templates(
            game_dir)
        print('Tiles and affordances loaded')
        self.sprites, self.sprite_affordances = utils.load_sprites_and_masks(
            game_dir)
        print('Sprites, masks, and affordances loaded')

    def init_filtered_images(self, image_path):
        self.orig_cv = cv2.imread(image_path, cv2.IMREAD_UNCHANGED)
        if self.orig_cv.shape[2] == 4:
            self.orig_cv = cv2.cvtColor(self.orig_cv, cv2.COLOR_BGRA2BGR)

        self.bgra_img = cv2.cvtColor(self.orig_cv, cv2.COLOR_BGR2BGRA)
        self.bgra_img = (32.0 * (self.bgra_img // (256 / 8))).astype('uint8')

        # print('unique values in image {}'.format(np.unique(self.bgra_img)))
        self.gray_img = cv2.cvtColor(self.bgra_img, cv2.COLOR_BGRA2BGR)
        self.gray_img = cv2.cvtColor(self.gray_img, cv2.COLOR_BGR2GRAY)

        grad_x = cv2.Sobel(self.gray_img, cv2.CV_64F, 1, 0, ksize=3)
        grad_y = cv2.Sobel(self.gray_img, cv2.CV_64F, 0, 1, ksize=3)
        abs_grad_x = cv2.convertScaleAbs(grad_x)
        abs_grad_y = cv2.convertScaleAbs(grad_y)
        self.sobel_img = cv2.addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0)

        dst = cv2.Laplacian(self.gray_img, cv2.CV_64F, ksize=3)
        self.laplace_img = cv2.convertScaleAbs(dst)

        self.canny_img = cv2.Canny(self.gray_img, 10, 300)

        self.img_copy = self.orig_PIL.copy()

        if self.UI_POSITION == 'top':
            temp_box = (0, self.UI_HEIGHT, self.WIDTH, self.HEIGHT)
        else:
            temp_box = (0, 0, self.WIDTH, self.HEIGHT-self.UI_HEIGHT)

        self.yolo_pil = self.orig_PIL.copy().crop(
            box=temp_box)
        # self.yolo_input = transforms.ToTensor()(self.yolo_pil)
        # self.yolo_input, _ = pad_to_square(self.yolo_input, 0)
        # self.yolo_input = resize(self.yolo_input, 416).unsqueeze(0)

    def detect_sprites(self):
        self.template_match_sprites()

    def template_match_sprites(self):
        print('template matching sprites')
        hits = {}
        sprite_locations_and_sizes = []
        for file_num, (rgba_sp, mask, gray_sp, g_mask, sobel_sp, laplace_sp, canny_sp) in self.sprites.items():
            res_a = cv2.matchTemplate(
                self.bgra_img, rgba_sp, cv2.TM_CCORR_NORMED, mask=mask)
            res_a = np.where(~np.isnan(res_a), res_a, 0)
            res_a = np.where(~np.isinf(res_a), res_a, 0)
            loc_a = np.where(res_a >= 0.85)
            minVal_a, maxVal_a, minLoc_a, maxLoc_a = cv2.minMaxLoc(res_a)
            matches_a = list((zip(*loc_a[::1])))

            res_g = cv2.matchTemplate(
                self.gray_img, gray_sp, cv2.TM_CCORR_NORMED, mask=g_mask)
            res_g = np.where(~np.isnan(res_g), res_g, 0)
            res_g = np.where(~np.isinf(res_g), res_g, 0)
            loc_g = np.where(res_g >= 0.94)
            minVal_g, maxVal_g, minLoc_g, maxLoc_g = cv2.minMaxLoc(res_g)
            matches_g = list((zip(*loc_g[::1])))

            res_s = cv2.matchTemplate(
                self.sobel_img, sobel_sp, cv2.TM_CCORR_NORMED, mask=g_mask)
            res_s = np.where(~np.isnan(res_s), res_s, 0)
            res_s = np.where(~np.isinf(res_s), res_s, 0)
            loc_s = np.where(res_s >= 0.83)
            minVal_s, maxVal_s, minLoc_s, maxLoc_s = cv2.minMaxLoc(res_s)
            matches_s = list((zip(*loc_s[::1])))

            res_l = cv2.matchTemplate(
                self.laplace_img, laplace_sp, cv2.TM_CCORR_NORMED, mask=g_mask)
            res_l = np.where(~np.isnan(res_l), res_l, 0)
            res_l = np.where(~np.isinf(res_l), res_l, 0)
            loc_l = np.where(res_l >= 0.85)
            minVal_l, maxVal_l, minLoc_l, maxLoc_l = cv2.minMaxLoc(res_l)
            matches_l = list((zip(*loc_l[::1])))

            # res_c = cv2.matchTemplate(
            #     self.canny_img, canny_sp, cv2.TM_CCORR_NORMED, mask=g_mask)
            # res_c = np.where(~np.isnan(res_c), res_c, 0)
            # res_c = np.where(~np.isinf(res_c), res_c, 0)
            # loc_c = np.where(res_c >= 0.6)
            # minVal_c, maxVal_c, minLoc_c, maxLoc_c = cv2.minMaxLoc(res_c)
            # matches_c = list((zip(*loc_c[::1])))

            # matches_all = list(set(matches_a) & set(matches_g) & set(
            #     matches_s) & set(matches_l) & set(matches_c))
            matches_all = list(set(matches_a) & set(
                matches_g) & set(matches_l) & set(matches_s))

            # if file_num in [262]:
            #     print("{:.3f} at {}, rgb {} ".format(
            #         maxVal_a,  maxLoc_a, file_num))
            #     print("{:.3f} at {}, gray".format(maxVal_g,  maxLoc_g))
            #     print("{:.3f} at {}, sobel".format(maxVal_s,  maxLoc_s))
            #     print("{:.3f} at {}, laplace".format(maxVal_l,  maxLoc_l))
            #     print("{:.3f} at {}, canny".format(maxVal_c,  maxLoc_c))
            #     print('.......................')

            hits[file_num] = (len(matches_all), maxVal_a,
                              maxVal_g, maxVal_l)
            h, w = gray_sp.shape[::1]

            if len(matches_all) > 0:
                sprite_affords = self.sprite_affordances.loc[self.sprite_affordances['file'] == file_num]
                list_affords = sprite_affords.values.tolist()

                list_affords = list_affords[0][1:]
                self.set_affordances_at_locations(
                    list_affords, matches_all, (h, w))
                # print('matched {}, size {} at locs {}. affords: {}'.format(
                #     file_num, gray_sp.shape, matches_all, list_affords))
                for pt in matches_all:
                    # self.image.create_line(
                    #     pt[0]*2, pt[1]*2, pt[0]*2+w*2, pt[1]*2, fill='lawn green', width=3, tags=('sprite'))
                    # self.image.create_line(
                    #     pt[0]*2, pt[1]*2, pt[0]*2, pt[1]*2+h*2, fill='lawn green', width=3, tags=('sprite'))
                    # self.image.create_line(
                    #     pt[0]*2+w*2, pt[1]*2, pt[0]*2+w*2, pt[1]*2+h*2, fill='lawn green', width=3, tags=('sprite'))
                    # self.image.create_line(
                    #     pt[0]*2, pt[1]*2+h*2, pt[0]*2+w*2, pt[1]*2+h*2, fill='lawn green', width=3, tags=('sprite'))
                    sprite_locations_and_sizes.append((pt, (h, w)))

                    # for y in range(pt[1], pt[1]+h):
                    #     for x in range(pt[0], pt[0]+w):
                    #         self.stacked_tensor[y, x] = torch.tensor(
                    #             list_affords, dtype=torch.float)
        sp_ctr = 0
        for x, y in hits.items():
            if y[0] != 0:
                sp_ctr += y[0]
                print('sprite num: ', x, ':', y[0])
                print("rgb: {:.3f} ".format(y[1]))
                print("gray: {:.3f}".format(y[2]))
                # print("sobel: {:.3f}".format(y[3]))
                print("laplace: {:.3f}".format(y[3]))
                # print("canny: {:.3f}".format(y[5]))
                print('.......................')
        print('{} sprites found locs: {}'.format(
            sp_ctr, len(sprite_locations_and_sizes)))
        self.sprite_locations_and_sizes.set(sprite_locations_and_sizes)

    def find_unique_tiles(self):
        print('Finding unique tiles in img')
        img_tiles = {}
        visited_locations = []
        tile_ctr = 0
        skip_ctr = 0
        for r in np.unique(self.rows):
            for c in np.unique(self.cols):
                if((r, c) not in visited_locations):
                    template_np = self.orig_cv[r:r+self.GRID_SIZE if r+self.GRID_SIZE <= self.HEIGHT else self.HEIGHT,
                                               c:c+self.GRID_SIZE if c+self.GRID_SIZE <= self.WIDTH else self.WIDTH]
                    res = cv2.matchTemplate(
                        template_np, self.orig_cv, cv2.TM_SQDIFF_NORMED)
                    loc = np.where(res <= 5e-6)
                    matches = list(zip(*loc[::1]))
                    matches = [(y, x) for (y, x) in matches if utils.point_on_grid(
                        x, y, self.cols, self.rows)]
                    if len(matches) != 0:
                        for match_loc in matches:
                            visited_locations.append(match_loc)
                    else:
                        print(
                            'ERROR MATCHING TILE WITHIN IMAGE: (r,c) ({},{})'.format(r, c))

                    img_tiles[tile_ctr] = ((template_np, matches))
                    tile_ctr += 1
                else:
                    skip_ctr += 1

        print('VISITED {} tiles, sum of unique({}) + skip({}) = {}'.format(
            len(visited_locations), len(img_tiles), skip_ctr, (len(img_tiles)+skip_ctr)))
        self.unique_tiles.set(img_tiles)

    def match_known_tiles(self, force=False, override_affordances=True):
        print('Comparing unique to known tiles')
        dupe_ctr = 0
        new_tiles = []
        ctr = 0
        print(type(self.unique_tiles.get()))
        for tile_num, (curr_tile, locations) in self.unique_tiles.get().items():

            is_new = True
            multi_match = False
            curr_tile_affords = []
            matches_on_tile = 0
            diffs = {}
            for name, old_tile in self.templates.items():
                if old_tile.shape == curr_tile.shape:
                    # res = cv2.matchTemplate(
                    #     old_tile, curr_tile, cv2.TM_SQDIFF_NORMED)
                    # loc = np.where(res <= 1e-3)
                    # minVal, maxVal, minLoc, maxLoc = cv2.minMaxLoc(res)
                    # matches = list(zip(*loc[::1]))
                    diff = np.square(np.subtract(curr_tile, old_tile))
                    total_diff = np.sum(diff)
                    denom = np.sqrt(np.multiply(
                        np.sum(np.square(curr_tile)), np.sum(np.square(old_tile))))
                    norm_diff = np.divide(total_diff, (denom + utils.E))

                    diffs[name] = norm_diff

            if len(diffs) > 0:
                best_match_value = min(diffs.values())
                best_match_files = [key for key,
                                    val in diffs.items() if val == best_match_value]

                #TODO: Reduce threshold or let user decide if they actually match
                if best_match_value < 0.01:
                    print('best match value: ', best_match_value,
                          ' on files: ', best_match_files)
                    dupe_ctr += 1
                    old_affords = self.template_affordances.loc[self.template_affordances['file']
                                                                == best_match_files[0]]
                    list_affords = old_affords.values.tolist()
                    list_affords = list_affords[0][1:]
                    #Get affordances and apply to locations
                    self.set_affordances_at_locations(
                        list_affords, locations, curr_tile.shape)
                else:
                    new_tiles.append((curr_tile, locations))
            else:
                new_tiles.append((curr_tile, locations))
            # for curr_tile, (c, r) in self.img_tiles:
            #     is_new = True
            #     for name, old_tile in self.templates.items():
            #         if old_tile.shape == curr_tile.shape:
            #             matches = []
            #
            #             res = cv2.matchTemplate(
            #                 old_tile, curr_tile, cv2.TM_SQDIFF_NORMED)
            #             loc = np.where(res <= .001)
            #             minVal, maxVal, minLoc, maxLoc = cv2.minMaxLoc(res)
            #             matches = list(zip(*loc[::-1]))
            #             if len(matches) != 0:
            #                 dupe_ctr += 1
            #                 is_new = False
            #         if self.match_all_old:
            #             old_affords = self.template_affordances.loc[self.template_affordances['file'] == name]
            #             list_affords = old_affords.values.tolist()
            #             list_affords = list_affords[0][1:]
            #             self.template_match_tile(old_tile, torch.tensor(
            #                 list_affords, dtype=torch.float), force)
            #     if self.match_all_old:
            #         col_num = self.width//self.GRID_SIZE
            #         if self.UI_POSITION == 'top':
            #             row_start = self.UI_HEIGHT // self.GRID_SIZE
            #             row_end = self.height // self.GRID_SIZE
            #             row_add = self.UI_HEIGHT % self.GRID_SIZE
            #         else:
            #             row_start = 0
            #             row_end = (self.height-self.UI_HEIGHT) // self.GRID_SIZE
            #             row_add = self.UI_HEIGHT % self.GRID_SIZE
            #         border_indices = [(m*self.GRID_SIZE+row_add, n*self.GRID_SIZE) for m in range(row_start, row_end)
            #                           for n in range(col_num) if m == row_start or m == row_end - 1 or n == 0 or n == col_num - 1]
            #         #Label non-solid edges as portal
            #         for m, n in border_indices:
            #             curr_solid = self.stacked_tensor[m, n, 0].item()
            #             if curr_solid == 0 or curr_solid == 0.5:
            #                 h = self.GRID_SIZE
            #                 for y in range(m, (m+h) if (m+h) <= (row_end * h) else (row_end * h)):
            #                     for x in range((n), (n)+self.GRID_SIZE):
            #                         self.stacked_tensor[y, x, 5] = 1.0
            #         torch.save(self.stacked_tensor, self.label_file)
            #     self.match_all_old = False
            #
            #     ctr += 1
            #     if is_new:
            #         self.new_tiles.append((curr_tile, (c, r)))

        print('new tiles', len(new_tiles), 'dupes', dupe_ctr)
        if len(new_tiles) > 0:
            print(new_tiles[0][1])
        self.new_tiles.set(new_tiles)

    def tag_ui(self):
        if self.UI_POSITION == 'top':
            row_start = 0
        else:
            row_start = self.HEIGHT-self.UI_HEIGHT-1
        self.set_affordances_at_locations(
            utils.UI_AFFORDANCE, [(row_start, 0)], (self.UI_HEIGHT, self.WIDTH))

    #TODO: make this less obnoxious, hopefully with set_affordances_at_locations, potentially using self.rows[0] or [-1] and self.cols
    def tag_border_portals(self):
        col_num = self.WIDTH//self.GRID_SIZE
        if self.UI_POSITION == 'top':
            row_start = self.UI_HEIGHT // self.GRID_SIZE
            row_end = self.HEIGHT // self.GRID_SIZE
            row_add = self.UI_HEIGHT % self.GRID_SIZE
        else:
            row_start = 0
            row_end = (self.HEIGHT-self.UI_HEIGHT) // self.GRID_SIZE
            row_add = self.UI_HEIGHT % self.GRID_SIZE
        border_indices = [(m*self.GRID_SIZE+row_add, n*self.GRID_SIZE) for m in range(row_start, row_end)
                          for n in range(col_num) if m == row_start or m == row_end - 1 or n == 0 or n == col_num - 1]
        #Label non-solid edges as portal
        for m, n in border_indices:
            curr_solid = self.stacked_tensor[m, n, 0].item()
            if curr_solid == 0 or curr_solid == 0.5:
                h = self.GRID_SIZE
                for y in range(m, (m+h) if (m+h) <= (row_end * h) else (row_end * h)):
                    for x in range((n), (n)+self.GRID_SIZE):
                        self.stacked_tensor[y, x, 5] = 1.0
        self.display_channels.set(self.stacked_tensor)

    def redo_template_match(self):
        self.rows, self.cols = utils.gen_grid(
            self.WIDTH, self.HEIGHT, self.GRID_SIZE, self.UI_HEIGHT, self.UI_POSITION,
            self.GRID_OFFSET_X, self.GRID_OFFSET_Y)
        self.find_unique_tiles()
        self.match_known_tiles()
        self.tag_border_portals()

    def askAffordances(self, tile, loc):
        print('ASKING AFFORDS FOR LOC ', loc)
        # thumb, locs = self.new_tiles[self.curr_tile_num.get()]
        # self.thumbnail_locations.set(locs)

        self.x0 = loc[1]
        self.y0 = loc[0]
        self.x1 = self.x0+tile.shape[0]
        self.y1 = self.y0+tile.shape[1]
        thumbnail = self.orig_PIL.crop(
            box=(self.x0, self.y0, self.x1, self.y1))
        self.thumbnail_image.set(thumbnail)

        # self.template_cv = self.orig_cv[self.y0:self.y1, self.x0:self.x1]
        # self.thumbnail_image = self.thumbnail_image.resize(
        #     (self.thumbnail_image.size[0]*4, self.thumbnail_image.size[1]*4))
        # if self.curr_tile_num == len(self.new_tiles)-1:
        #     self.curr_tile_num.set(0)
        # else:
        #     self.curr_tile_num.set(self.curr_tile_num.get() + 1)

    # def flipAffordance(self, idx):
    #     temp = self.curr_affordances.get()
    #     print('flipping affordance: ', idx, temp[idx])
    #     temp[idx] = (temp[idx] + 1) % 2
    #     self.curr_affordances.set(temp)

    def confirmAffordances(self, list_affords):
        print(type(list_affords), list_affords)
        for r in range(self.y0, self.y1):
            for c in range(self.x0, self.x1):
                # print('updating {},{}'.format(r, c))
                self.stacked_tensor[r, c] = torch.tensor(
                    list_affords, dtype=torch.float)
        self.display_channels.set(self.stacked_tensor)
        torch.save(self.stacked_tensor, self.label_file)

    def set_affordances_at_locations(self, list_affords, locations, shape, overwrite=True):
        # print('setting shape: ', shape, '. at locations ',
        #       locations[0], '. ', len(locations), 'total locations', '. aff:', list_affords)
        for (y, x) in locations:
            for r in range(y, y + shape[0]):
                for c in range(x, x + shape[1]):
                    if self.stacked_tensor[r, c, 0].item() == 0.5 or overwrite:
                        # print('updating {},{}'.format(r, c))
                        self.stacked_tensor[r, c] = torch.tensor(
                            list_affords, dtype=torch.float)
        self.display_channels.set(self.stacked_tensor)
        torch.save(self.stacked_tensor, self.label_file)

    def insert_in_csv(self, affordance_entry, game_dir, data_path):
        idx = 0
        ser = pd.Series(self.template_affordances['file'])
        while True:
            if idx not in ser.values:
                new_series = pd.Series(
                    [idx, *affordance_entry], index=self.template_affordances.columns)
                self.template_affordances = self.template_affordances.append(
                    new_series, ignore_index=True)
                print(self.template_affordances.tail(2))
                self.template_affordances.to_csv(
                    os.path.join(
                        data_path, game_dir, 'tile_affordances.csv'), index=False)
                return (self.template_affordances.shape[0]-1, idx)
            idx += 1


def parse_args():
    parser = argparse.ArgumentParser(description='Affordance Tagger')
    # directory args, default structure has tagging_tool folder and games folder
    #  at the same level
    parser.add_argument('--data-path', type=str, default='../games',
                        help='path to games folder')
    parser.add_argument('--file-num', type=int, default=0,
                        help='image num to tag')
    parser.add_argument('--game-dir', type=str, default='sm3',
                        help='game directory name')

    # override args for initializing grid and UI. -1 fetches defaults from utils
    parser.add_argument('--ui-height', type=int, default=-1,
                        help='height of ui from nearest edge of screen in pixels')
    parser.add_argument('--grid-size', type=int, default=-1,
                        help='size of grid to use in pixels')
    parser.add_argument('--ui-position', type=str, default='default',
                        help='top or bot. where to ignore ui')

    # args for YOLO detector. Requires additional files
    parser.add_argument("--model-def", type=str,
                        default="yolo/config/yolov3-vg.cfg", help="path to model definition file")
    parser.add_argument("--weights-path", type=str,
                        default="yolo/checkpoints/yolov3_custom_ckpt_18.pth", help="path to weights file")
    parser.add_argument("--class-path", type=str,
                        default="yolo/data/custom/classes.names", help="path to class label file")
    parser.add_argument("--conf-thres", type=float,
                        default=0.8, help="object confidence threshold")
    parser.add_argument("--nms-thres", type=float, default=0.4,
                        help="iou thresshold for non-maximum suppression")
    args = parser.parse_args()

    return args


if __name__ == '__main__':
    args = parse_args()
    while True:
        app = AffordanceTagger(args)
        app.run()
        args.file_num += 1
        #break
